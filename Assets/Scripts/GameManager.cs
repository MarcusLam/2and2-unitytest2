﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Collider colliderExit;

	public SmileyGuy smileyGuy;
	public GameObject victoryPanel;

	public Text victoryPanelTimeText;
	public Text timeText;

	public Timer timer;

	// Use this for initialization
	void Start () {
		victoryPanel.SetActive(false);
		smileyGuy.onEventTriggerEnter += HandleSmileyGuyTriggerEnter;
	}

	void HandleSmileyGuyTriggerEnter(Collider collider) {
		if (collider == colliderExit) {
			EndGame();
		}
	}
	
	void EndGame() {
		timer.active = false;
		smileyGuy.Kill();
		victoryPanel.SetActive(true);
	}

	// Update is called once per frame
	void Update () {
		victoryPanelTimeText.text = timer.timeInSeconds;
		timeText.text = timer.timeInSeconds;
	}

}