﻿using UnityEngine;
using System.Collections;

public class SmileyGuy : MonoBehaviour {

	private bool isAlive;
	private Rigidbody rigidBody;

	// Events.
	public delegate void EventTriggerEnter(Collider collider);
	public event EventTriggerEnter onEventTriggerEnter;

	// Use this for initialization
	void Start () {
		isAlive = true;
		rigidBody = gameObject.GetComponent<Rigidbody>();
	}
	
	public void Kill() {
		isAlive = false;
		rigidBody.velocity = new Vector3(0, 0, 0);
	}

	void OnTriggerEnter(Collider collider) {
 		onEventTriggerEnter(collider);
 	}

	// Update is called once per frame
	void Update () {
		if (!isAlive) return;
		Vector3 position = transform.position;
		float speed = 0.03f;
		if (Input.GetKey(KeyCode.W)) rigidBody.velocity += new Vector3(0, -speed, 0);
		if (Input.GetKey(KeyCode.A)) rigidBody.velocity += new Vector3(-speed, 0, 0);
		if (Input.GetKey(KeyCode.S)) rigidBody.velocity += new Vector3(0, speed, 0);
		if (Input.GetKey(KeyCode.D)) rigidBody.velocity += new Vector3(speed, 0, 0);
		transform.position = position;
	}

}