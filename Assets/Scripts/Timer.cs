﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	public int counter;
	public bool active;
	public string timeInSeconds;

	// Use this for initialization
	void Start () {
		active = true;
		counter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (!active) return;
		int counter = 0;
		counter++;
		timeInSeconds = (counter / 60) + " seconds";
	}

}